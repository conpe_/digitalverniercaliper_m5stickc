## 概要
500円くらいで売っているデジタルノギスの信号を、M5StickCで読み取って表示する。

![概要](./resource/readm5.jpg)

## 環境
* M5StickC
* platformio

## デジタルノギスの仕様
### ハードウェア
ノギスの基板からは、クロックとデータ信号が出ます。  
![ピン](./resource/pin.jpg)  
画像左から、GND、クロック、データ。  
信号の電圧は約1.5V。

### データフォーマット
#### 全体波形
波形の全体図を示します。
赤がクロック、黄色がデータ。  
0mmのとき  
![全体波形0mm](./resource/overview_0.jpg)  
160mmのとき  
![全体波形160mm](./resource/overview_160.jpg)  
全体では24ビットあります。  
これが150mmほど空けて繰り返されます。  
![周期](./resource/cycle.jpg)  

#### データの読み取りタイミング
図は0mm時の拡大図です。  
クロックの間隔は400us。  
![拡大0mm](./resource/enlarge_0.jpg)  
続いてほんの少しだけ動かしたときの波形。いくつかのビットが1になっていることがわかります。  
![拡大数ミリ](./resource/enlarge_1.jpg)  
この違いから、クロックが立ち上がったタイミングでデータを読めば良さそうであることが分かります。  
少し動かしたときに先頭の方のビットが変化したので、LSBから順に送られてきているようです。

#### 単位、分解能
ノギス本体のボタンを押すことで、ミリ表示とインチ表示を切り替えられます。これに合わせて出力されるデータも変わります。  
最後の1ビットが0のときメートル系(0.01mm単位)、1のときインチ系(0.0005インチ単位)です。  
こちらは約160mmに合わせたときのミリ表示での波形。  
![インチビット](./resource/milli_160mm.jpg)  
BIN : 0011 1110 1000 0010  
HEX : 0x3E82  
DEC : 16002  
1ビットあたり0.01mmなので、160.02mmと読むことができます。

続いては同じく160mm(=6.3インチ)におけるインチ表示。  
![インチビット](./resource/milli_6p3inch.jpg)  
BIN : 0011 0001 0011 1000  
HEX : 0x3138  
DEC : 12600  
1ビットあたり0.0005インチなので、6.3インチですね。  

ちなみに21ビット目が負号を示します。0のとき正。1のとき負です。

## ソフト
### 概要
クロックは400us秒周期なので割り込みで問題ないでしょう。クロック信号の立ち上がりで割り込みをかけます。(回路で信号を反転させているので実際には立ち下がり)  
前回のクロックから時間が立っているときにデータの先頭だと判断します。ここでは1msとしました。  
24ビット分割り込みが入ったら読み終わりです。符号を判定して、値を確定させます。

### ポート設定
* クロック信号 PIN_DVC_CLOCK
* データ信号 PIN_DVC_DATA

M5StickCだとG0,G26,G36のポートが使いやすいですが、次の罠があります。
* G36は内蔵プルアップ不可。外付けする必要あり
* G0は起動時にLowになっていると書き込みモードになってしまう


## 参考サイト
[デジタルノギスのデーターをArduinoで読んでみた](http://radiopench.blog96.fc2.com/blog-entry-324.html)
