#include <Arduino.h>
#include <M5StickC.h>

/* 接続ポート */
/* M5StickCでの注意点 */
/* * G36は内蔵プルアップ不可。外付けする必要あり */
/* * G0は起動時にLowになっていると書き込みモードになってしまう */
#define PIN_DVC_CLOCK 26
#define PIN_DVC_DATA 36

int16_t mes_val = 0;  /* 0.01mm単位 */

void isr_clock_in(void);

void setup() {
  M5.begin();
  M5.Lcd.fillScreen(BLUE);
  M5.Lcd.setRotation(1);
  
  M5.Lcd.setTextFont(4);
  M5.Lcd.setTextColor(YELLOW, BLUE);
  M5.Lcd.print("Digital\n    Vernier\n      Caliper");

  pinMode(PIN_DVC_CLOCK, INPUT_PULLUP);
  pinMode(PIN_DVC_DATA, INPUT_PULLUP);

  attachInterrupt(PIN_DVC_CLOCK, isr_clock_in, FALLING); /* トランジスタで反転させているので立ち下がりエッジで判定 */

  delay(1500);
}

void loop() {
  static int16_t mes_val_last = 999;

  M5.update();

  if(mes_val != mes_val_last){
    M5.Lcd.fillScreen(BLUE);
    M5.Lcd.setTextFont(4);
    M5.Lcd.setTextColor(YELLOW, BLUE);
    M5.Lcd.setTextSize(1);
    M5.Lcd.setTextDatum(5);   /* right center 効かない */
    M5.Lcd.setCursor(M5.Lcd.width() - 5, 10);
    M5.Lcd.printf("%3.2f", mes_val/100.0f);

    mes_val_last = mes_val;
  }
}

void isr_clock_in(void){
  static uint32_t last = 0;         /* 前回時刻[us] */
  uint32_t m = micros();            /* 今回時刻[us] */
  uint32_t clock_width = m - last;  /* クロック幅[us] */
  static uint32_t rcvbit = 0x0001;  /* 受信するビット位置 */
  static uint32_t rcvdata;          /* 受信中データ */
  static bool receiving = false;    /* 受信中？ */

  /* データ頭出し */
  if(clock_width > 1000){ /* クロック幅が長い */
    rcvbit = 0x0001;      /* 先頭ビット */
    rcvdata = 0x0000;     /* データ初期化 */
    receiving = true;
  }

  if(receiving){
    /* データ読む */
    if(!digitalRead(PIN_DVC_DATA)){ /* トランジスタで反転させているので反転 */
      rcvdata |= rcvbit;
    }
    rcvbit = rcvbit << 1;           /* 次のビットへ */
    
    /* 24bit読み終わり */
    if(rcvbit>=(0x00000001<<24)){
      receiving = false;              /* 受信完了 */

      mes_val = rcvdata & 0x00007FFF; /* signed16bitまで使えば十分 ±320.00[mm] */
      if(rcvdata&0x00100000){         /* 20bit目が立ってる = 負値 */
        mes_val = -mes_val;
      }
    }
  }

  last = m;
}

